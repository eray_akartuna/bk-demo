FROM node:latest AS node
FROM php:7.3-fpm

COPY composer.json /var/www/
COPY package.json /var/www/

WORKDIR /var/www

RUN apt-get update && apt-get install -y \
    libzip-dev\
    git \
    zip \
    unzip \
    curl

COPY --from=node /usr/local/lib/node_modules /usr/local/lib/node_modules
COPY --from=node /usr/local/bin/node /usr/local/bin/node
RUN ln -s /usr/local/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /var/www

RUN chmod -R 777 /var/www/storage

VOLUME ["/var/www"]

CMD bash -c "npm install && npm run dev && composer install && php artisan serve --host 0.0.0.0 --port 80"
