# Frameworks
Backend: Laravel
Frontend: Vuejs

### Folders

<p>You can find Vue files in the "resources" folder.</p>
<p>You can find Unit test files in the "tests/Unit" folder.</p>
<p>You can find Integration test files in the "tests/Feature" folder.</p>

# Installation

### Run
docker-compose up -d

### Schema Installation
docker exec -it my_demo_app_php-fpm php artisan migrate

### Connecting to database

<p>docker exec -it my_demo_mysql_db bash</p>
<p>mysql -uroot -p</p>
<p>Password is "secret"</p>
<p>Type "use homestead;"</p>
<p>Now you are able to run sqls. Let's try "show tables;"</p>


# Automated Tests

<p>I didn't cover all cases because it takes time. But you can see some examples</p>
<p>Run unit tests by "docker exec -it my_demo_app_php-fpm vendor/bin/phpunit tests/Unit"</p>
<p>Run integration tests by "docker exec -it my_demo_app_php-fpm vendor/bin/phpunit tests/Feature"</p>


# Application Usage

<p>URL: http://localhost:8080/#/</p>
<p>Create new order/subscription http://localhost:8080/#/create-order</p>
<p>List subscription, change iteration, create new order accordingly subscription date: http://localhost:8080/#/subscriptions</p>
<p>List orders http://localhost:8080/#/orders</p>
<p>List customers: http://localhost:8080/#/customers</p>
