<?php

namespace App\Enums;

/**
 * Class StatusEnums
 * @package App\Enums
 */
final class StatusEnums
{
    const ACTIVE = 1;
    const INACTIVE = 0;

    const CREATED_ORDER = 0;
    const FAILED_ORDER = -1;
    const PAID_ORDER = 1;
}
