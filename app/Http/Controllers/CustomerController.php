<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerListRequest;
use App\Repository\CustomerRepository;
use Illuminate\Support\Collection;

/**
 * Class CustomerController
 * @package App\Http\Controllers
 */
class CustomerController extends Controller
{
    protected $customerRepository;

    /**
     * CustomerController constructor.
     * @param CustomerRepository $customerRepository
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param CustomerListRequest $customerListRequest
     * @return array
     */
    public function all(CustomerListRequest $customerListRequest): array
    {
        return ['customers' => $this->customerRepository->getAll($customerListRequest)];
    }

    /**
     * @return Collection
     */
    public function actives(): Collection
    {
        return $this->customerRepository->getActives();
    }
}
