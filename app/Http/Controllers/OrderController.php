<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Repository\OrderRepository;
use App\Services\OrderService;
use App\ValueObjects\OrderStatus;
use Illuminate\Http\Request;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    protected $orderService;
    protected $orderRepository;

    /**
     * OrderController constructor.
     * @param OrderService $orderService
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderService $orderService, OrderRepository $orderRepository)
    {
        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return ['orders' => $this->orderRepository->getAll()];
    }

    /**
     * @param StoreOrderRequest $request
     * @return array
     */
    public function store(StoreOrderRequest $request): array
    {
        return ['status' => $this->orderService->saveOrder($request)];
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function createNextOrder(Request $request)
    {
        return ['status' => $this->orderService->createOrderBySubscription($request->get('subscriptionId'))];
    }

    /**
     * @param Request $request
     */
    public function changeStatus(Request $request)
    {
        $this->orderRepository->changeStatus($request->get('id'), new OrderStatus($request->get('status')));
    }
}
