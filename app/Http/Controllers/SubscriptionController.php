<?php

namespace App\Http\Controllers;

use App\Repository\SubscriptionRepository;
use Illuminate\Http\Request;

/**
 * Class SubscriptionController
 * @package App\Http\Controllers
 */
class SubscriptionController extends Controller
{
    protected $subscriptionRepository;

    /**
     * SubscriptionController constructor.
     * @param SubscriptionRepository $subscriptionRepository
     */
    public function __construct(SubscriptionRepository $subscriptionRepository)
    {
        $this->subscriptionRepository = $subscriptionRepository;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return ['subscriptions' => $this->subscriptionRepository->getAll()];
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function setIterationFrequency(Request $request): array
    {
        $this->subscriptionRepository->saveIteration($request->get('id'), $request->get('iteration'));

        return ['status' => true];
    }
}
