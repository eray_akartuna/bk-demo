<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CustomerListRequest
 * @package App\Http\Requests
 */
class CustomerListRequest extends FormRequest
{
    /**
     * @return false
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function hasFilter(): bool
    {
        return $this->get('filter', 0) != 0;
    }

    /**
     * @return bool
     */
    public function isMultiplePaidOrderFilter(): bool
    {
        return $this->get('filter') == 1;
    }

    /**
     * @return bool
     */
    public function isHasSubscriptionFilter(): bool
    {
        return $this->get('filter') == 2;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return ['filter' => 'in:0,1,2'];
    }
}
