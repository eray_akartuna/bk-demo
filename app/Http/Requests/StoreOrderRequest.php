<?php

namespace App\Http\Requests;

use App\Model\Customer;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreOrderRequest
 * @package App\Http\Requests
 */
class StoreOrderRequest extends FormRequest
{
    /**
     * @return false
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isNewCustomer(): bool
    {
        return $this->get('customerId', 0) === 0;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return array_merge($this->getCustomerRules(), $this->getOrderRules());
    }

    /**
     * @return string[]
     */
    private function getCustomerRules(): array
    {
        if (!$this->isNewCustomer()) {
            return ['customerId' => 'required|int|exists:' . Customer::class . ',id'];
        }

        return ['email' => 'required|email|max:200|unique:customers', 'name' => 'required|max:200'];
    }

    /**
     * @return string[]
     */
    private function getOrderRules(): array
    {
        $rules = ['total' => 'required|min:1|integer'];

        if ($this->get('hasSubscription')) {
            $rules['iteration'] = 'required|min:1|integer';
        }

        return $rules;
    }
}
