<?php

namespace App\Model;

use App\Enums\StatusEnums;
use App\Traits\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Customer
 * @package App\Model
 * @property int id
 * @property string name
 * @property string email
 * @property bool isActive
 * @property string statusLabel
 */
class Customer extends Model
{
    use Status;

    protected $fillable = ['name', 'email', 'isActive'];
    protected $casts = ['isActive' => 'boolean'];
    protected $table = 'customers';
    protected $appends = ['statusLabel'];

    /**
     * @return HasMany
     */
    public function subscriptions(): HasMany
    {
        return $this->hasMany(Subscription::class, 'customerId');
    }

    /**
     * @return HasMany
     */
    public function activeSubscriptions(): HasMany
    {
        return $this->subscriptions()->where('isActive', StatusEnums::ACTIVE);
    }

    /**
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'customerId');
    }

    /**
     * @return HasOne
     */
    public function lastPaidOrder(): HasOne
    {
        return $this->hasOne(Order::class, 'customerId')->orderBy('id', 'desc');
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute(): string
    {
        return $this->getStatusLabel($this->attributes['isActive']);
    }
}
