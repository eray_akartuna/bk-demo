<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Order
 * @package App\Model
 * @property string name
 * @property string email
 * @property bool isActive
 */
class Order extends Model
{
    protected $fillable = ['customerId', 'subscriptionId', 'total'];
    protected $casts = ['paidDate' => 'datetime:Y-m-d'];
    protected $appends = ['statusLabel'];
    protected $table = 'orders';

    /**
     * @return BelongsTo
     */
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class, 'customerId');
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute(): string
    {
        return __('app.orderStatus.' . $this->attributes['status']);
    }
}
