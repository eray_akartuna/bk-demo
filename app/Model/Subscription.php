<?php

namespace App\Model;

use App\Traits\Status;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Subscription
 * @package App\Model
 * @property string name
 * @property string email
 * @property int dayIteration
 * @property Carbon nextOrderDate
 * @property bool isActive
 */
class Subscription extends Model
{
    use Status;

    protected $fillable = ['customerId', 'startDate', 'nextOrderDate', 'dayIteration', 'is'];
    protected $casts = [
        'isActive' => 'boolean',
        'startDate' => 'datetime:Y-m-d',
        'nextOrderDate' => 'datetime:Y-m-d',
    ];
    protected $table = 'subscriptions';
    protected $appends = ['statusLabel'];

    /**
     * @return BelongsTo
     */
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class, 'customerId');
    }

    /**
     * @return HasOne
     */
    public function order(): HasOne
    {
        return $this->hasOne(Order::class, 'subscriptionId');
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute(): string
    {
        return $this->getStatusLabel($this->attributes['isActive']);
    }
}
