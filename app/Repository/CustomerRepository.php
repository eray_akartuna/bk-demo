<?php

namespace App\Repository;

use App\Enums\StatusEnums;
use App\Http\Requests\CustomerListRequest;
use App\Model\Customer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class CustomerRepository
 * @package App\Repository
 */
class CustomerRepository
{
    protected $model;

    /**
     * CustomerRepository constructor.
     * @param Customer $customer
     */
    public function __construct(Customer $customer)
    {
        $this->model = $customer;
    }

    /**
     * @return Collection|null
     */
    public function getActives(): ?Collection
    {
        return $this->model->where('isActive', StatusEnums::ACTIVE)->get();
    }

    /**
     * @param CustomerListRequest $customerListRequest
     * @return Collection|null
     */
    public function getAll(CustomerListRequest $customerListRequest): ?Collection
    {
        $query = $this->model->withCount(['activeSubscriptions', 'orders'])->with('lastPaidOrder')->orderBy('id', 'desc');

        if ($customerListRequest->hasFilter()) {
            $paidCount = $customerListRequest->isMultiplePaidOrderFilter() ? 2 : 1;

            $query->whereHas('orders', function (Builder $query) {
                $query->where('status', StatusEnums::PAID_ORDER);
            }, '>=', $paidCount);
        }

        if ($customerListRequest->isHasSubscriptionFilter()) {
            $query->has('activeSubscriptions');
        }

        return $query->get();
    }

    /**
     * @param string $name
     * @param string $email
     * @return Customer
     */
    public function save(string $name, string $email): Customer
    {
        return $this->model->create(compact('name', 'email'));
    }
}
