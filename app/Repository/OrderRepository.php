<?php

namespace App\Repository;

use App\Model\Order;
use App\ValueObjects\OrderStatus;
use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class OrderRepository
 * @package App\Repository
 */
class OrderRepository
{
    protected $model;

    /**
     * OrderRepository constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->model = $order;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->model->with('customer')->orderBy('id', 'desc')->get();
    }

    /**
     *
     * @param int $id
     * @param OrderStatus $status
     * @return bool
     */
    public function changeStatus(int $id, OrderStatus $status): bool
    {
        $paidDate = null;

        if ($status->isPaid()) {
            $paidDate = Carbon::today();
        }

        return $this->model->where('id', $id)->update(['status' => $status->get(), 'paidDate' => $paidDate]);
    }

    /**
     * @param int $customerId
     * @param int $total
     * @param int|null $subscriptionId
     * @return bool
     */
    public function create(int $customerId, int $total, ?int $subscriptionId): bool
    {
        return $this->model->insert(compact('customerId', 'total', 'subscriptionId'));
    }

    /**
     * @param int $subscriptionId
     * @return Order
     */
    public function getLastOrderBySubscription(int $subscriptionId): Order
    {
        return $this->model->orderBy('id', 'desc')->where('subscriptionId', $subscriptionId)->first();
    }
}
