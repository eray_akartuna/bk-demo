<?php

namespace App\Repository;

use App\Model\Subscription;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

/**
 * Class CustomerRepository
 * @package App\Repository
 */
class SubscriptionRepository
{
    protected $model;

    /**
     * SubscriptionRepository constructor.
     * @param Subscription $subscription
     */
    public function __construct(Subscription $subscription)
    {
        $this->model = $subscription;
    }

    /**
     * @return Collection|null
     */
    public function getAll(): ?Collection
    {
        return $this->model->with('customer')->get();
    }

    /**
     * @param int $customerId
     * @param int $iteration
     * @return Subscription
     */
    public function create(int $customerId, int $iteration): Subscription
    {
        $startDate = Date::now();

        return $this->model->create([
            'customerId' => $customerId,
            'dayIteration' => $iteration,
            'startDate' => $startDate,
            'nextOrderDate' => $startDate->clone()->addDays($iteration),
        ]);
    }

    /**
     * @param int $id
     * @param int $iteration
     * @throws \Exception
     */
    public function saveIteration(int $id, int $iteration): void
    {
        DB::beginTransaction();

        try {
            /** @var Subscription $subscription */
            $subscription = $this->model->lockForUpdate()->find($id);
            $lastOrderDate = $subscription->nextOrderDate->clone()->addDays(-1 * $subscription->dayIteration);
            $nextOrderDate = $lastOrderDate->addDays($iteration);

            if ($nextOrderDate->isPast()) {
                $nextOrderDate = Date::today();
            }

            $subscription->dayIteration = $iteration;
            $subscription->nextOrderDate = $nextOrderDate;
            $subscription->save();

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param int $id
     * @return Subscription
     * @throws \Exception
     */
    public function iterateNextOrderDate(int $id): Subscription
    {
        DB::beginTransaction();

        try {
            /** @var Subscription $subscription */
            $subscription = $this->model->lockForUpdate()->find($id);
            $subscription->nextOrderDate = $subscription->nextOrderDate->clone()->addDays($subscription->dayIteration);
            $subscription->save();

            DB::commit();

            return $subscription;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
