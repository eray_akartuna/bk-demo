<?php

namespace App\Services;

use App\Http\Requests\StoreOrderRequest;
use App\Repository\CustomerRepository;
use App\Repository\OrderRepository;
use App\Repository\SubscriptionRepository;

/**
 * Class OrderService
 * @package App\Services
 */
class OrderService
{
    protected $subscriptionRepository;
    protected $customerRepository;
    protected $orderRepository;

    /**
     * OrderService constructor.
     * @param SubscriptionRepository $subscriptionRepository
     * @param CustomerRepository $customerRepository
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        SubscriptionRepository $subscriptionRepository,
        CustomerRepository $customerRepository,
        OrderRepository $orderRepository
    ) {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->customerRepository = $customerRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param StoreOrderRequest $request
     * @return bool
     */
    public function saveOrder(StoreOrderRequest $request): bool
    {
        $customerId = $request->get('customerId');
        $subscriptionId = null;

        if ($request->isNewCustomer()) {
            $customer = $this->customerRepository->save($request->get('name'), $request->get('email'));

            if (!$customer) {
                return false;
            }

            $customerId = $customer->id;
        }

        if ($request->get('hasSubscription')) {
            $subscription = $this->subscriptionRepository->create($customerId, $request->get('iteration'));

            if (!$subscription) {
                return false;
            }

            $subscriptionId = $subscription->id;
        }

        return $this->orderRepository->create($customerId, $request->get('total'), $subscriptionId);
    }

    /**
     * @todo i might use db transaction...
     * @param int $subscriptionId
     * @return bool
     * @throws \Exception
     */
    public function createOrderBySubscription(int $subscriptionId): bool
    {
        $subscription = $this->subscriptionRepository->iterateNextOrderDate($subscriptionId);

        if (!$subscription) {
            return false;
        }

        $order = $this->orderRepository->getLastOrderBySubscription($subscriptionId);

        if (!$order) {
            return false;
        }

        return $this->orderRepository->create($subscription->customerId, $order->total, $subscription->id);
    }
}
