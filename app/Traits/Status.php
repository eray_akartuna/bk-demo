<?php

namespace App\Traits;

/**
 * Trait Status
 * @package App\Traits
 */
trait Status
{
    /**
     * @param int $status
     * @return string
     */
    public function getStatusLabel(int $status): string
    {
        return __("app.status.{$status}");
    }
}
