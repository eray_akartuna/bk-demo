<?php

namespace App\ValueObjects;

use App\Enums\StatusEnums;

/**
 * Class OrderStatus
 * @package App\ValueObjects
 */
class OrderStatus
{
    protected $status;

    /**
     * OrderStatus constructor.
     * @param int $status
     */
    public function __construct(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->status === StatusEnums::PAID_ORDER;
    }

    /**
     * @return int
     */
    public function get(): int
    {
        return $this->status;
    }
}
