<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'isActive' => $faker->boolean,
    ];
});


$factory->state(Customer::class, 'active', ['isActive' => true]);
$factory->state(Customer::class, 'inactive', ['isActive' => false]);
