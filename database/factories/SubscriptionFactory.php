<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Customer;
use App\Model\Subscription;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'customerId' => factory(Customer::class)->create(),
        'startDate' => Carbon::now()->format('Y-m-d'),
        'nextOrderDate' => Carbon::tomorrow()->format('Y-m-d'),
        'dayIteration' => $faker->randomNumber(1, 1000),
    ];
});

$factory->state(Subscription::class, 'active', ['isActive' => true]);
$factory->state(Subscription::class, 'inactive', ['isActive' => false]);
