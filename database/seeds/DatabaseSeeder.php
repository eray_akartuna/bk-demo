<?php

use App\Model\Customer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Customer::class)->create();
        // $this->call(UserSeeder::class);
    }
}
