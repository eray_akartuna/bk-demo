import Vue from 'vue';
import './bootstrap.js';
import Index from './Views/Index.vue';
import router from './routes.js';

new Vue({
    el: "#page",
    router,
    components: {
        Index
    }
});
