import Vue from 'vue';
import Router from 'vue-router';
import Home from './Pages/Home';
import Order from './Pages/Order';
import Customers from './Pages/Customers';
import Subscriptions from './Pages/Subscriptions';
import Orders from './Pages/Orders';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/create-order',
            name: 'Order',
            component: Order
        },
        {
            path: '/customers',
            name: 'Customers',
            component: Customers
        },
        {
            path: '/subscriptions',
            name: 'Subscriptions',
            component: Subscriptions
        },
        {
            path: '/orders',
            name: 'Orders',
            component: Orders
        }
    ]
});
