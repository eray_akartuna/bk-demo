<?php

use App\Enums\StatusEnums;

return [
    'status' => [
        StatusEnums::ACTIVE => 'Active',
        StatusEnums::INACTIVE => 'Inactive',
    ],
    'orderStatus' => [
        StatusEnums::CREATED_ORDER => 'Created',
        StatusEnums::FAILED_ORDER => 'Failed',
        StatusEnums::PAID_ORDER => 'Paid',
    ],
];
