<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <style>
            table, thead, tbody, tr, td {
                border: 1px solid #636b6f;
            }
        </style>
    </head>
    <div id="page">
        <index></index>
    </div>

    <script src="{{ mix('js/app.js') }}" ></script>
</html>
