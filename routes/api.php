<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/customers', 'CustomerController@all');
Route::get('/customers/actives', 'CustomerController@actives');

Route::group(['prefix' => 'subscriptions'], function () {
    Route::get('/', 'SubscriptionController@all');
    Route::patch('/set-frequency', 'SubscriptionController@setIterationFrequency');
});

Route::group(['prefix' => 'order'], function () {
    Route::get('/', 'OrderController@all');
    Route::post('/', 'OrderController@store');
    Route::post('/create-next-order', 'OrderController@createNextOrder');
    Route::patch('/change-status', 'OrderController@changeStatus');
});
