<?php

namespace Tests\Feature\Controllers;

use App\Model\Customer;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class CustomerControllerTest
 * @package Tests\Feature\Controllers
 * @coversDefaultClass \App\Http\Controllers\CustomerController
 */
class CustomerControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     * @covers ::__construct
     * @covers ::all
     */
    function it_should_return_customers()
    {
        factory(Customer::class, 5)->state('active')->create();
        $response = $this->get('api/customers');

        $response->assertOk();
    }
}
