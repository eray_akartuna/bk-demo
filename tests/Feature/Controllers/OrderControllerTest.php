<?php

namespace Tests\Feature\Controllers;

use App\Model\Customer;
use App\Model\Order;
use App\Model\Subscription;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Date;
use Tests\TestCase;

/**
 * Class OrderControllerTest
 * @package Tests\Feature\Controllers
 * @coversDefaultClass \App\Http\Controllers\OrderController
 */
class OrderControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * @test
     * @covers ::store
     */
    function it_should_save_order()
    {
        Date::setTestNow('now');

        $name = $this->faker->name;
        $email = $this->faker->unique()->email;
        $total = $this->faker->numberBetween(1, 1000);
        $iteration = $this->faker->numberBetween(1, 100);
        $hasSubscription = true;

        $response = $this->post(
            'api/order',
            compact('name', 'email', 'total', 'iteration', 'hasSubscription')
        );

        $response->assertOk()->assertExactJson(['status' => true]);
        $this->assertDatabaseHas('customers', compact('name', 'email'));
        $this->assertDatabaseHas(
            'subscriptions',
            [
                'dayIteration' => $iteration,
                'startDate' => Date::now()->format('Y-m-d'),
                'nextOrderDate' => Date::now()->addDays($iteration)->format('Y-m-d'),
                'isActive' => true,
            ]
        );
        $this->assertDatabaseHas('orders', ['total' => $total, 'status' => 0]);
    }
}
