<?php

namespace Tests\Feature\Controllers;

use App\Model\Subscription;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Date;
use Tests\TestCase;

/**
 * Class SubscriptionControllerTest
 * @package Tests\Feature\Controllers
 * @coversDefaultClass \App\Http\Controllers\SubscriptionController
 */
class SubscriptionControllerTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    /**
     * @test
     * @covers ::setIterationFrequency
     */
    function it_should_save_iteration()
    {
        Date::setTestNow('yesterday');

        $iteration = 40;
        $subscription = factory(Subscription::class)
            ->create(['dayIteration' => 10, 'startDate' => Date::now(), 'nextOrderDate' => Date::now()->addDays(10)]);

        $this->patch('api/subscriptions/set-frequency', ['id' => $subscription->id, 'iteration' => $iteration]);

        $this->assertDatabaseHas(
            'subscriptions',
            ['nextOrderDate' => Date::now()->addDays(40), 'id' => $subscription->id, 'dayIteration' => $iteration]
        );
    }

    /**
     * @test
     * @covers ::setIterationFrequency
     */
    function it_should_save_next_order_date_today_when_next_order_date_is_past()
    {
        Date::setTestNow('now');

        $iteration = 10;
        $subscription = factory(Subscription::class)
            ->create([
                'dayIteration' => 20,
                'startDate' => Date::now()->addDays(-15),
                'nextOrderDate' => Date::now()->addDays(5),
            ]);

        $this->patch('api/subscriptions/set-frequency', ['id' => $subscription->id, 'iteration' => $iteration]);

        $this->assertDatabaseHas(
            'subscriptions',
            ['nextOrderDate' => Date::today(), 'id' => $subscription->id, 'dayIteration' => $iteration]
        );
    }
}
