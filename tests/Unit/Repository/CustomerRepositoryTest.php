<?php

namespace Tests\Unit\Repository;

use App\Model\Customer;
use App\Repository\CustomerRepository;
use Tests\TestCase;

/**
 * Class CustomerRepositoryTest
 * @package Tests\Unit\Repository
 * @coversDefaultClass \App\Repository\CustomerRepository
 */
class CustomerRepositoryTest extends TestCase
{
    /**
     * @test
     * @covers ::__construct
     * @covers ::getAll
     */
    function it_should_return_all_customers()
    {
        $customers = collect([factory(Customer::class)->make()]);
        $model = \Mockery::mock(Customer::class);

        $model->shouldReceive('where')->with('isActive', 1)->once()->andReturnSelf();
        $model->shouldReceive('get')->once()->andReturn($customers);

        $this->assertEquals($customers, (new CustomerRepository($model))->getActives());
    }
}
