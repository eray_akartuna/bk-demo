<?php

namespace Tests\Unit\Requests;

use App\Http\Requests\StoreOrderRequest;
use App\Model\Customer;
use Tests\TestCase;

/**
 * Class StoreOrderRequestTest
 * @package Tests\Unit\Requests
 * @coversDefaultClass \App\Http\Requests\StoreOrderRequest
 */
class StoreOrderRequestTest extends TestCase
{
    /**
     * @test
     * @covers ::rules
     */
    function it_should_return_rules_with_new_customer_when_customer_id_is_empty()
    {
        $request = new StoreOrderRequest(['customerId' => 0]);

        $this->assertEquals(
            [
                'email' => 'required|email|max:200|unique:customers',
                'name' => 'required|max:200',
                'total' => 'required|min:1|integer',
            ],
            $request->rules()
        );
    }

    /**
     * @test
     * @covers ::rules
     */
    function it_should_return_rules_with_customer_id_when_customer_id_is_not_empty()
    {
        $request = new StoreOrderRequest(['customerId' => 5]);

        $this->assertEquals(
            [
                'customerId' => 'required|int|exists:' . Customer::class . ',id',
                'total' => 'required|min:1|integer',
            ],
            $request->rules()
        );
    }

    /**
     * @test
     * @covers ::rules
     */
    function it_should_return_rules_with_subscription_iteration_when_has_subscription()
    {
        $request = new StoreOrderRequest(['hasSubscription' => true, 'customerId' => 0]);

        $this->assertEquals(
            [
                'email' => 'required|email|max:200|unique:customers',
                'name' => 'required|max:200',
                'iteration' => 'required|min:1|integer',
                'total' => 'required|min:1|integer',
            ],
            $request->rules()
        );
    }
}
